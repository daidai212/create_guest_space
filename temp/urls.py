# -*- coding: UTF-8 -*-
"""temp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from app import views

# 实验室服务器添加普通用户的模块
# from app import add_user
# from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^main/$', views.main_index),
    url(r'^people/$', views.people_introduce),
    url(r'^equip/$', views.equip_data),
    url(r'^space/$', views.space_data),
    url(r'^equip/change/$', views.equip_data_change),
    url(r'^space/change/$', views.space_data_change)
    # url(r'^spaces/', views.space_data_change)
]
# 实验室服务器添加普通用户
#  url(r'^add_user/$', add_user.AddUser),
# urlpatterns += staticfiles_urlpatterns()

# 这个注释没用
