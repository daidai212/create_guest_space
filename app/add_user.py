# ! /usr/bin/env python
# -*- coding: utf-8 -*-
# import sys
# reload(sys)#如果没有这个模块会报错
# sys.setdefaultencoding('utf-8')
# 实验室服务器添加普通用户
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators import csrf
# 加载os模块用于执行文件
import os


def AddUser(request):
    status = {}
    if not(request.method == "POST"):
        status['return_status'] = ''
        return render(request, 'add_user.html', status)
    if request.method == "POST":
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        # 添加成功的情况
        if password.isalnum():  # 判断password是字母和数字的混合
            # 下面操作文件去创建用户
            # 打开文件
            fileObject = open("/home/dh/adduser.sh", "w+")
            # 打开第二行文件
            file_line2 = fileObject.readline(2)
            file_line2 = file_line2.replace("zhangsan", username)
            # 打开第三行文件
            file_line3 = fileObject.readline(3)
            file_line3 = file_line2.replace("zhangsan", username)
            # 打开第七行文件
            file_line7 = fileObject.readline(7)
            file_line7 = file_line2.replace("123456", password)
            # 关闭文件
            fileObject.close()
            # 执行替换后的文件
            executable_file = "/home/dh/adduser.sh"  # 可执行文件为
            os.system(executable_file)
            status['return_status'] = '用户添加成功！'
            status['username'] = "您添加的用户为：" + username
            status['password'] = "您添加用户的密码为：" + password
            return render(request, 'add_user.html', status)
        else:
            # 添加失败的情况
            status['return_status'] = '添加失败，请使用其他用户名或者复杂的密码重新尝试！'
            return render(request, "add_user.html", status)


# 替换的文件源码如下 文件在实验室服务器的/home/dh/adduser.sh
"""
#!/bin/bash
USER_COUNT=`cat /etc/passwd | grep '^zhangsan:' -c`
USER_NAME='zhangsan'
if [ $USER_COUNT -ne 1  ]
then
    useradd $USER_NAME
    echo "123456" | passwd $USER_NAME --stdin
    else
    echo 'user exits'
fi

"""