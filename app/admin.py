from django.contrib import admin

# Register your models here.
from django.contrib import admin
from app.models import PeopleIntroduce
from app.models import EquipData
from app.models import SpaceData

# Register your models here.
admin.site.register(PeopleIntroduce)
admin.site.register(EquipData)
admin.site.register(SpaceData)
