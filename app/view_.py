# -*- coding: UTF-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from app.models import PeopleIntroduce
from app.models import EquipData
from app.models import SpaceData
from . import models

# Create your views here.

# return render(request,'index.html')


def main_index(request):
    return render(request, 'index.html')


def people_introduce(request):
    if request.GET.get("search_name") is not None:
        search_name = request.GET['search_name']
        peoples = PeopleIntroduce.objects.filter(name=search_name)
        if len(peoples) > 0:
            return render(request, 'people.html', {"peoples": peoples})
        else:
            error_content = {'error': '抱歉，您所搜索的内容不存在！'}
            return render(request, 'people.html', error_content)
    else:
        peoples = PeopleIntroduce.objects.all()
        return render(request, 'people.html', {"peoples": peoples})


def equip_data(request):
    if request.method == "GET":
        if request.GET.get("search_name") is not None:
            name = request.GET['search_name']
            equips = EquipData.objects.filter(name=name)
            if len(equips) > 0:
                return render(request, 'equip.html', {"equips": equips})
            else:
                error_content = {'error': '抱歉，您所搜索的内容不存在！'}
                return render(request, 'equip.html', error_content)
        else:
            equips = EquipData.objects.all()
            return render(request, 'equip.html', {"equips": equips})
    elif request.method == "POST":
        equips = EquipData.objects.all()
        name = request.POST['name']
        equip_status = request.POST['equip_status']
        borrow_number = request.POST['borrow_number']
        borrow_time = request.POST['borrow_time']
        return_time = request.POST['return_time']
        remarks = request.POST['remarks']
        if equip_status == 'True':
            if len(name) == 0:
                status = {'status': "请输入设备名!"}
                return render(request, 'equip.html', status, {"equips": equips})
            else:
                equipment = EquipData.objects.filter(name=name)
                if len(equipment) == 0:
                    return render(request, 'equip.html', {'status': "设备不存在！", "equips": equips})
                else:
                    if len(equipment.filter(state=True)) > 0:
                        return render(request, 'equip.html', {'status': "设备已在库！", "equips": equips})
                    else:
                        # equipment.state = equip_status
                        # equipment.borrow_number = []
                        # equipment.borrow_time = []
                        # equipment.return_time = []
                        # equipment.remarks = remarks
                        equipment.update(state=equip_status)
                        equipment.update(borrow_number=borrow_number)
                        equipment.update(borrow_time=borrow_time)
                        equipment.update(return_time=return_time)
                        equipment.update(remarks=remarks)
                        return render(request, 'equip.html', {'status': "归还成功！", "equips": equips})
        else:
            if (len(name) == 0) or (len(borrow_number) == 0) or (len(borrow_time) == 0) or (len(return_time) == 0):
                return render(request, 'equip.html', {'status': "请输入完整的信息！", "equips": equips})
            else:
                equipment = EquipData.objects.filter(name=name)
                if len(equipment) == 0:
                    return render(request, 'equip.html', {'status': "设备不存在！", "equips": equips})
                else:
                    if len(equipment.filter(state=False)) > 0:
                        return render(request, 'equip.html', {'status': "设备已借出！", "equips": equips})
                    else:
                        # equipment.state = equip_status
                        # equipment.borrow_number = borrow_number
                        # equipment.borrow_time = borrow_time
                        # equipment.return_time = return_time
                        # equipment.remarks = remarks
                        equipment.update(state=equip_status)
                        equipment.update(borrow_number=borrow_number)
                        equipment.update(borrow_time=borrow_time)
                        equipment.update(return_time=return_time)
                        equipment.update(remarks=remarks)
                        return render(request, 'equip.html', {'status': "借取成功！", "equips": equips})


def space_data(request):
    if request.method == "GET":
        if request.GET.get("search_name") is not None:
            name = request.GET['search_name']
            spaces = SpaceData.objects.filter(name=name)
            if len(spaces) > 0:
                return render(request, 'space.html', {"spaces": spaces})
            else:
                error_content = {'error': '抱歉，您所搜索的内容不存在！'}
                return render(request, 'space.html', error_content)
        else:
            spaces = SpaceData.objects.all()
            return render(request, 'space.html', {"spaces": spaces})
    elif request.method == "POST":
        spaces = SpaceData.objects.all()
        name = request.POST['name']
        space_status = request.POST['space_status']
        borrow_number = request.POST['borrow_number']
        borrow_time = request.POST['borrow_time']
        return_time = request.POST['return_time']
        remarks = request.POST['remarks']
        if space_status == 'True':
            if len(name) == 0:
                status = {'status': "请输入场地名!"}
                return render(request, 'space.html', status, {"spaces": spaces})
            else:
                place = SpaceData.objects.filter(name=name)
                if len(place) == 0:
                    return render(request, 'space.html', {'status': "场地不存在！", "spaces": spaces})
                else:
                    if len(place.filter(state=True)) > 0:
                        return render(request, 'equip.html', {'status': "场地已空余！", "spaces": spaces})
                    else:
                        # equipment.state = equip_status
                        # equipment.borrow_number = []
                        # equipment.borrow_time = []
                        # equipment.return_time = []
                        # equipment.remarks = remarks
                        place.update(state=space_status)
                        place.update(borrow_number=borrow_number)
                        place.update(borrow_time=borrow_time)
                        place.update(return_time=return_time)
                        place.update(remarks=remarks)
                        return render(request, 'space.html', {'status': "归还成功！", "spaces": spaces})
        else:
            if (len(name) == 0) or (len(borrow_number) == 0) or (len(borrow_time) == 0) or (len(return_time) == 0):
                return render(request, 'space.html', {'status': "请输入完整的信息！", "spaces": spaces})
            else:
                place = SpaceData.objects.filter(name=name)
                if len(place) == 0:
                    return render(request, 'space.html', {'status': "场地不存在！", "spaces": spaces})
                else:
                    if len(place.filter(state=False)) > 0:
                        return render(request, 'space.html', {'status': "场地已借出！", "spaces": spaces})
                    else:
                        # equipment.state = equip_status
                        # equipment.borrow_number = borrow_number
                        # equipment.borrow_time = borrow_time
                        # equipment.return_time = return_time
                        # equipment.remarks = remarks
                        place.update(state=space_status)
                        place.update(borrow_number=borrow_number)
                        place.update(borrow_time=borrow_time)
                        place.update(return_time=return_time)
                        place.update(remarks=remarks)
                        return render(request, 'space.html', {'status': "借取成功！", "spaces": spaces})
