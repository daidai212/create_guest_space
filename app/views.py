# -*- coding: UTF-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from app.models import PeopleIntroduce
from app.models import EquipData
from app.models import SpaceData
from . import models

# Create your views here.

# return render(request,'index.html')


def main_index(request):
    return render(request, 'index.html')


def people_introduce(request):
    if request.GET.get("search_name") is not None:
        search_name = request.GET['search_name']
        peoples = PeopleIntroduce.objects.filter(name=search_name)
        if len(peoples) > 0:
            return render(request, 'people.html', {"peoples": peoples})
        else:
            error_content = {'error': '抱歉，您所搜索的内容不存在！'}
            return render(request, 'people.html', error_content)
    else:
        peoples = PeopleIntroduce.objects.all()
        return render(request, 'people.html', {"peoples": peoples})


def equip_data(request):
    if request.GET.get("search_name") is not None:
        name = request.GET['search_name']
        equips = EquipData.objects.filter(name=name)
        if len(equips) > 0:
            return render(request, 'equip.html', {"equips": equips})
        else:
            error_content = {'error': '抱歉，您所搜索的内容不存在！'}
            return render(request, 'equip.html', error_content)
    else:
        equips = EquipData.objects.all()
        return render(request, 'equip.html', {"equips": equips})


def space_data(request):
    if request.GET.get("search_name") is not None:
        name = request.GET['search_name']
        spaces = SpaceData.objects.filter(name=name)
        if len(spaces) > 0:  # 数据库中存在
            return render(request, 'space.html', {"spaces": spaces})
        else:  # 数据库中不存在
            error_content = {'error': '抱歉，您所搜索的内容不存在！'}
            return render(request, 'space.html', error_content)
    else:
        spaces = SpaceData.objects.all()
        return render(request, 'space.html', {"spaces": spaces})


def equip_data_change(request):
    name = request.POST['name']
    equip_status = request.POST['equip_status']
    borrow_number = request.POST['borrow_number']
    borrow_time = request.POST['borrow_time']
    return_time = request.POST['return_time']
    remarks = request.POST['remarks']
    if (name is not None) and (borrow_number is not None) and (borrow_time is not None) and (return_time is not None):
        # 输入不正确
        status = {'status': '抱歉，您输入错误！'}
        return render(request, 'space.html', status)
    else:  # 输入正确
        if EquipData.objects.filter(name=name) is not None:  # 设备在数据库中存在
            data = EquipData.objects.filter(name=name)
            if equip_status is True:  # 还
                if data.state is True:  # 设备存在
                    status = {"status": "抱歉，您所归还的设备已经归还！"}
                    return render(request, "equip.html", status)
                else:  # 设备不存在
                    # 正确归还
                    data.state = equip_status
                    data.borrow_number = borrow_number
                    data.borrow_time = borrow_time
                    data.return_time = return_time
                    data.remarks = remarks
                    status = {"status": "归还成功！"}
                    return render(request, "equip.html", status)
            else:  # 借
                if data.state is True:  # 场地存在
                    # 正确借取
                    data.state = equip_status
                    data.borrow_number = borrow_number
                    data.borrow_time = borrow_time
                    data.return_time = return_time
                    data.remarks = remarks
                    status = {"status": "借取成功！"}
                    return render(request, "space.html", status)
                else:  # 场地不存在
                    status = {"status": "抱歉，您所归还的设备不存在！"}
                    return render(request, "equip.html", status)
        else:  # 设备在数据库中不存在
            status = {"status": "抱歉，您所归还的设备不存在！"}
            return render(request, "equip.html", status)

"""
def space_data_change(request):
    data = list(SpaceData.objects.filter(name='242424').values('state'))
    add = data[0]
    abc = add['state']
    abcd = (abc != 'False')
    return HttpResponse(abcd)
"""


def space_data_change(request):
    space_name = request.POST['name']
    space_status = request.POST['space_status']
    borrow_number = request.POST['borrow_number']
    borrow_time = request.POST['borrow_time']
    return_time = request.POST['return_time']
    remarks = request.POST['remarks']
    if (len(space_name) == 0) and (len(borrow_number) == 0) and (len(borrow_time) == 0) and (len(return_time) == 0):
        # 输入不正确
        status = {'status': '抱歉，您输入错误！'}
        return render(request, 'space.html', status)
    else:  # 输入正确
        try:  # 查询数据库中的全部name和输入的space_name有没有匹配的
            # 数据库中存在
            data = SpaceData.objects.filter(name=space_name)
            data_list = list(SpaceData.objects.filter(name=space_name).values('state'))
            ab = data_list[0]
            abc = ab['state']
            # data_or = list(SpaceData.objects.filter(name=space_name))  # 获取链表结果
            # data = SpaceData.objects.filter(name=space_name).values('state')  # 获取字典结果state
            true = "True"  # 为后面判断space_status的boolean
            if space_status == true:  # 输入的是归还
                if abc == 'False':  # 数据库中不存在
                    # 正确归还
                    try:
                        data.update(state='True')
                        data.update(borrow_number=borrow_number)
                        data.update(borrow_time=borrow_time)
                        data.update(return_time=return_time)
                        data.update(remarks=remarks)
                        status = {'status': '归还成功！'}
                        return render(request, 'space.html', status)
                    except SpaceData.DoesNotExist:
                        # 归还失败
                        status = {'status': '抱歉，归还失败！'}
                        return render(request, 'space.html', status)
                else:  # 场地存在
                    # 不能归还
                    status = {'status': '抱歉，该场地已经归还！'}
                    return render(request, 'space.html', status)
            else:  # 输入的是借取
                if abc == 'True':  # 数据库中存在
                    # 正确借取
                    try:
                        data.update(state='False')
                        data.update(borrow_number=borrow_number)
                        data.update(borrow_time=borrow_time)
                        data.update(return_time=return_time)
                        data.update(remarks=remarks)
                        status = {'status': '借取成功！'}
                        return render(request, 'space.html', status)
                    except SpaceData.DoesNotExist:
                        # 借取失败
                        status = {'status': '抱歉，借取失败！'}
                        return render(request, 'space.html', status)
                else:  # 场地不存在
                    # 不能借取
                    status = {'status': '抱歉，该场地已经借出！'}
                    return render(request, 'space.html', status)
        except models.SpaceData.DoesNotExist:  # 数据库中不存在
            status = {'status': '抱歉，您输入的场地不存在！'}
            return render(request, 'space.html', status)



"""
        if len(SpaceData.objects.get(name="space_name")) > 0:  # 场地在数据库中存在
            data = SpaceData.objects.filter(name=space_name)
            if space_status is True:  # 还
                status = {"status": "归还成功！"}
                return render(request, "space.html", status)
                if data.state is True:  # 场地存在
                    status = {"status": "抱歉，您所归还的场地已经归还！"}
                    return render(request, "space.html", status)
                else:  # 场地不存在
                    # 正确归还
                    data.state = space_status
                    data.borrow_number = borrow_number
                    data.borrow_time = borrow_time
                    data.return_time = return_time
                    data.remarks = remarks
                    status = {"status": "归还成功！"}
                    return render(request, "space.html", status)
            else:  # 借
                if data.state is True:  # 场地存在
                    # 正确借取
                    data.state = space_status
                    data.borrow_number = borrow_number
                    data.borrow_time = borrow_time
                    data.return_time = return_time
                    data.remarks = remarks
                    status = {"status": "借取成功！"}
                    return render(request, "space.html", status)
                else:  # 场地不存在
                    status = {"status": "抱歉，您所借取的场地不存在！"}
                    return render(request, "space.html", status)
        else:  # 场地在数据库中不存在
            status = {"status": "抱歉，您所归还的场地不存在！"}
            return render(request, "space.html", status)
"""


"""
                try:
                    if data.state == 'False':  # 数据库中不存在
                    # 正确归还
                    data.update(state='True')
                    data.update(borrow_number=borrow_number)
                    data.update(borrow_time=borrow_time)
                    data.update(return_time=return_time)
                    data.update(remarks=remarks)
                    status = {'status': '归还成功！'}
                    return render(request, 'space.html', status)
                except SpaceData.DoesNotExist:  # 已经归还
                    # 归还失败
                    status = {'status': '抱歉，你所归还的设备已经归还！'}
                    return render(request, 'space.html', status)
                """