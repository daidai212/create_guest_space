# -*- coding: UTF-8 -*-

# from django.utils import timezone
from django.db import models
# from django.contrib.models import User


class PeopleIntroduce(models.Model):
    name = models.CharField(max_length=10)
    introduce = models.TextField()


class EquipData(models.Model):
    name = models.CharField(max_length=50)
    state = models.BooleanField()  # 是否在,True为存在没有被借出
    borrow_number = models.IntegerField()  # 借取人学号
    borrow_time = models.CharField(max_length=20)  # 借取时间
    return_time = models.CharField(max_length=20)   # 归还时间
    remarks = models.TextField(max_length=50)   # 备注


class SpaceData(models.Model):
    name = models.CharField(max_length=50)
    state = models.BooleanField()  # 存在的话就是true
    borrow_number = models.IntegerField()
    borrow_time = models.CharField(max_length=20)
    return_time = models.CharField(max_length=20)
    remarks = models.TextField(max_length=50)
